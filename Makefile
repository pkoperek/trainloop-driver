VERSION_TAG="1.0.dev"

docker:
	docker build -f Dockerfile -t pkoperek/trainloop-driver:${VERSION_TAG} .

docker-push: docker
	docker tag pkoperek/trainloop-driver:${VERSION_TAG} pkoperek/trainloop-driver:latest
	docker push pkoperek/trainloop-driver:${VERSION_TAG}
	docker push pkoperek/trainloop-driver:latest

dev: docker
	docker-compose -f docker-compose-dev.yml down
	docker-compose -f docker-compose-dev.yml rm -f
	docker-compose -f docker-compose-dev.yml up

integration-test: docker
	docker-compose -f docker-compose-integration-tests.yml down
	docker-compose -f docker-compose-integration-tests.yml rm -f
	docker-compose -f docker-compose-integration-tests.yml up

train-sample: docker
	docker-compose -f docker-compose-train-sample.yml down
	docker-compose -f docker-compose-train-sample.yml rm -f
	docker-compose -f docker-compose-train-sample.yml up

train-dqn: docker
	docker-compose -f docker-compose-train-dqn.yml down
	docker-compose -f docker-compose-train-dqn.yml rm -f
	docker-compose -f docker-compose-train-dqn.yml up --no-color | tee train.log

phd: docker
	docker-compose --no-ansi -f docker-compose-phd.yml down
	docker-compose --no-ansi -f docker-compose-phd.yml rm -f
	docker-compose --no-ansi -f docker-compose-phd.yml up
