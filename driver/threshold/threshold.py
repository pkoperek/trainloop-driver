import re

from driver.common import set_global_seeds
from driver.common.runners import AbstractEnvRunner
from driver import logger


RULE = re.compile("(\d+):([<>=]=?)(\d+\.\d+)->(\d+)")

# observation metrics - all within 0-1 range
# 0 "vmAllocatedRatioHistory",
# 1 "avgCPUUtilizationHistory",
# 2 "p90CPUUtilizationHistory",
# 3 "avgMemoryUtilizationHistory",
# 4 "p90MemoryUtilizationHistory",
# 5 "waitingJobsRatioGlobalHistory",
# 6 "waitingJobsRatioRecentHistory"

# Actions

# case 0:
#    // nothing happens
#    break;
# case 1:
#    // adding a new vm
#    addNewVM(CloudSimProxy.SMALL);
#    break;
# case 2:
#    // removing randomly one of the vms
#    removeVM(CloudSimProxy.SMALL);
#    break;
# case 3:
#    addNewVM(CloudSimProxy.MEDIUM);
#    break;
# case 4:
#    removeVM(CloudSimProxy.MEDIUM);
#    break;
# case 5:
#    addNewVM(CloudSimProxy.LARGE);
#    break;
# case 6:
#    removeVM(CloudSimProxy.LARGE);
#    break;


class Rule:
    """
    rule format:
    <OBSERVATION_IDX>:<OPERATOR><VALUE>-><ACTION>
    <RULE>(";"<RULE>)
    """

    def __init__(self, observation_index, operator, threshold_value, action):
        self.observation_index = observation_index
        self.operator = operator
        self.threshold_value = threshold_value
        self.action = action

    def is_triggered(self, observations):
        # we assume that we always have a single environment
        observation = observations[0][self.observation_index]

        if self.operator == "=" or self.operator == "==":
            return observation == self.threshold_value

        if self.operator == ">":
            return observation > self.threshold_value

        if self.operator == "<":
            return observation < self.threshold_value

        if self.operator == "<=":
            return observation <= self.threshold_value

        if self.operator == ">=":
            return observation >= self.threshold_value

        raise RuntimeError(f"Unknown operator {self.operator}!")

    @staticmethod
    def parse_rule(rule_as_str):
        match = RULE.match(rule_as_str)
        if match:
            observation_index = int(match.group(1))
            operator = match.group(2)
            threshold_value = float(match.group(3))
            action = int(match.group(4))
            logger.info(f"Parsed: {observation_index} {operator} {threshold_value} {action}")
            return Rule(
                observation_index=observation_index,
                operator=operator,
                threshold_value=threshold_value,
                action=action,
            )
        else:
            logger.error(f"Rule: {rule_as_str} has incorrect format!")
            return None

    @staticmethod
    def parse_rules(rules_as_str):

        rules_as_lst = rules_as_str.split(";")

        parsed_rules = []
        for rule_as_str in rules_as_lst:
            rule = Rule.parse_rule(rule_as_str)
            if rule:
                parsed_rules.append(rule)

        return parsed_rules


class ThresholdModel:

    def __init__(self, ac_space, rules):
        self.initial_state = {}
        self.ac_space = ac_space
        self.rules = rules

    def step(self, obs):
        obs_type = type(obs)
        #logger.info(f"Observations: {obs} {obs_type}")
        for rule in self.rules:
            if rule.is_triggered(obs):
                action = rule.action
                logger.info(f"Observations: {obs[0]} Action: {action}")
                return action

        # if no rule triggers - we do nothing
        return 0

    def save(self, save_path):
        logger.info(f"Storing of the model to {save_path} requested. Ignoring...")


class SimpleRunner(AbstractEnvRunner):

    def __init__(self, *, env, model, nsteps):
        super().__init__(env=env, model=model, nsteps=nsteps)

    def run(self):
        for _ in range(self.nsteps):
            action = self.model.step(self.obs)
            self.obs[:], _, done, _ = self.env.step(action)

            if done:
                logger.info("The episode is done...")


def learn(env, seed, total_timesteps, rules, nsteps=2048, **kwargs):
    set_global_seeds(seed)

    nenvs = env.num_envs

    ob_space = env.observation_space
    ac_space = env.action_space

    logger.info(f"Starting random algorithm (nsteps: {nsteps})")
    logger.info(f"Environment: {env}")
    logger.info(f"Observation space: {ob_space}")
    logger.info(f"Action space: {ac_space}")

    parsed_rules = Rule.parse_rules(rules)

    model = ThresholdModel(ac_space=ac_space, rules=parsed_rules)
    runner = SimpleRunner(env=env, model=model, nsteps=nsteps)
    epochs = total_timesteps // nenvs // nsteps

    for i in range(epochs):
        logger.info(f"Epoch {i}")
        runner.run()

    return model


if __name__ == "__main__":
    print("Valid")
    print(Rule.parse_rules("0:<0.5->3"))
    print(Rule.parse_rules("0:<0.0->3"))
    print(Rule.parse_rules("0:<=0.5->3"))
    print(Rule.parse_rules("0:<0.5->3"))
    print(Rule.parse_rules("0:>0.5->3"))
    print(Rule.parse_rules("0:>=0.5->3"))
    print(Rule.parse_rules("11:>1.5->3"))
    print(Rule.parse_rules("11:=1.5->3"))
    print(Rule.parse_rules("11:==1.5->3"))
    print(Rule.parse_rules("0:==1.5->3;1:>10.0->2"))
    print("Invalid")
    print(Rule.parse_rules("0:=<0.5->3"))
    print(Rule.parse_rules("0:<0.5->"))
    print(Rule.parse_rules("0:<0.5-3"))
    print(Rule.parse_rules("0:<0.5>3"))
    print(Rule.parse_rules("0:<5->3"))
    print(Rule.parse_rules(":<5.0->3"))
    print(Rule.parse_rules("0:<5.->3"))
