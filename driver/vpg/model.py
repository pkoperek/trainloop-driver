import tensorflow as tf
import functools

from driver.common.tf_util import get_session, save_variables, load_variables
from driver.common.tf_util import initialize

try:
    from driver.common.mpi_adam_optimizer import MpiAdamOptimizer
    from mpi4py import MPI
    from driver.common.mpi_util import sync_from_root
except ImportError:
    MPI = None

class Model(object):
    """
    We use this object to :
    __init__:
    - Creates the step_model
    - Creates the train_model

    train():
    - Make the training part (feedforward and retropropagation of gradients)

    save/load():
    - Save load the model
    """
    def __init__(self, *, policy, ob_space, ac_space, nbatch_act, nbatch_train,
                nsteps, ent_coef, vf_coef, max_grad_norm, mpi_rank_weight=1, comm=None, microbatch_size=None):
        self.sess = sess = get_session()

        if MPI is not None and comm is None:
            comm = MPI.COMM_WORLD

        with tf.variable_scope('vpg_model', reuse=tf.AUTO_REUSE):
            # CREATE OUR TWO MODELS
            # act_model that is used for sampling
            act_model = policy(nbatch_act, 1, sess)

            # Train model for training
            if microbatch_size is None:
                train_model = policy(nbatch_train, nsteps, sess)
            else:
                train_model = policy(microbatch_size, nsteps, sess)

        # CREATE THE PLACEHOLDERS
        self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ADV = ADV = tf.placeholder(tf.float32, [None])
        self.R = R = tf.placeholder(tf.float32, [None])
        self.LR = LR = tf.placeholder(tf.float32, [])

        neglogpac = train_model.pd.neglogp(A)

        # VF_LOSS
        vf_loss = tf.reduce_mean(tf.square(R - train_model.vf))
        pi_loss = tf.reduce_mean(neglogpac * ADV)

        # UPDATE THE PARAMETERS USING LOSS
        # 1. Get the model parameters
        params = tf.trainable_variables('vpg_model')
        print(f"Trainable variables: {params}")

        if comm is not None and comm.Get_size() > 1:
            self.vf_trainer = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
            self.pi_trainer = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        else:
            self.vf_trainer = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)
            self.pi_trainer = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)

        # Calculate VF gradients
        vf_grads_and_var = self.vf_trainer.compute_gradients(vf_loss, params)
        self._vf_train_op = self.vf_trainer.apply_gradients(vf_grads_and_var)

        # Calculate PI gradients
        pi_grads_and_var = self.pi_trainer.compute_gradients(pi_loss, params)
        self._pi_train_op = self.pi_trainer.apply_gradients(pi_grads_and_var)

        self.loss_names = ['policy_loss', 'value_loss']
        self.stats_list = [pi_loss, vf_loss]

        self.train_model = train_model
        self.act_model = act_model
        self.step = act_model.step
        self.value = act_model.value
        self.initial_state = act_model.initial_state

        self.save = functools.partial(save_variables, sess=sess)
        self.load = functools.partial(load_variables, sess=sess)

        initialize()
        global_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="")
        if MPI is not None:
            sync_from_root(sess, global_variables, comm=comm) #pylint: disable=E1101

    def train(self, lr, obs, returns, masks, actions, values, states=None):
        # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
        # Returns = R + yV(s')
        advs = returns - values

        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)

        td_map = {
            self.train_model.X : obs,
            self.A : actions,
            self.ADV : advs,
            self.R : returns,
            self.LR : lr,
        }

        if states is not None:
            td_map[self.train_model.S] = states
            td_map[self.train_model.M] = masks

        ret_val = self.sess.run(
            self.stats_list + [self._pi_train_op, self._vf_train_op],
            td_map
        )[:-2]

        print(f"Model train: {ret_val}")

        for _ in range(80):
            self.sess.run(
                self.stats_list + [self._vf_train_op],
                td_map
            )
        return ret_val
