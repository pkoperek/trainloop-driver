from driver.common import set_global_seeds
from driver.common.runners import AbstractEnvRunner
from driver import logger


def learn(env, seed, total_timesteps, constant_action_id="0", nsteps=2048, **kwargs):
    set_global_seeds(seed)

    # Get the nb of env
    nenvs = env.num_envs

    # Get state_space and action_space
    ob_space = env.observation_space
    ac_space = env.action_space

    class SimpleRunner(AbstractEnvRunner):

        def __init__(self, *, env, model, nsteps):
            super().__init__(env=env, model=model, nsteps=nsteps)

        def run(self):
            for _ in range(self.nsteps):
                action = self.model.step()
                logger.info(f"Executing action: {action}")
                _, _, done, _ = self.env.step(action)

                if done:
                    logger.info("The episode is done...")

    logger.info(f"Starting constant algorithm (nsteps: {nsteps}) ")
    logger.info(f"Observation space: {ob_space}")
    logger.info(f"Action space: {ac_space}")

    class ConstantModel:

        def __init__(self, ac_space, action_to_execute):
            self.initial_state = {}
            self.ac_space = ac_space
            self.action_to_execute = action_to_execute

        def step(self):
            return self.action_to_execute

        def save(self, save_path):
            logger.info(f"Storing of the model to {save_path} requested. Ignoring...")

    model = ConstantModel(ac_space=ac_space, action_to_execute=int(constant_action_id))

    # Instantiate the runner object
    runner = SimpleRunner(env=env, model=model, nsteps=nsteps)

    epochs = total_timesteps // nenvs // nsteps

    logger.info(f"epochs: {epochs} total_timesteps: {total_timesteps} nenvs: {nenvs} nsteps: {nsteps}")
    for i in range(epochs):
        runner.run() #pylint: disable=E0632

    return model
